#!/bin/sh -x
O=`dirname "$0"`/
. ${O}config || exit 1

main() {
 ${O}build &&
 {
  run
  for TRACK in keimola germany usa france; do
   run $TRACK
  done
 } |
 tee -a run.log
}

run() {
 date
# $O/run
 local P="$PWD"
 cd $O$PL || exit 1
 ./run testserver.helloworldopen.com 8091 "$BOTNAME" "$BOTKEY" "$@"
 local S=$?
 mv log.txt log.`date +%s`.txt
 cd "$P"
 return $S
}

main "$@"

