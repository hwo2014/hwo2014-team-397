package hwo2014.amateurs

import org.json4s.{JValue, DefaultFormats}
import java.net.Socket
import java.io._
import org.json4s.native.Serialization
import scala.annotation.tailrec

import hwo2014.amateurs.model._
import hwo2014.amateurs.message._
import hwo2014.amateurs.ai.SimpleAI

import org.json4s.JsonAST.{JValue, JNothing}
import hwo2014.amateurs.message.JoinQuick
import scala.Some
import hwo2014.amateurs.message.BotId
import hwo2014.amateurs.message.JoinRace
import hwo2014.amateurs.model.CarData


object NoobBot extends App {
  args.toList match {
    case hostName :: port :: botName :: botKey :: trackName :: _ =>
      new NoobBot(hostName, Integer.parseInt(port), botName, botKey, trackName)
    case hostName :: port :: botName :: botKey :: _ =>
      new NoobBot(hostName, Integer.parseInt(port), botName, botKey)
    case _ => println("args missing")
  }
}

class NoobBot(host: String, port: Int, botName: String, botKey: String, trackName: String = "") {

  implicit val formats = new DefaultFormats{}
  val log = new PrintWriter(new OutputStreamWriter(new FileOutputStream("log.txt"), "UTF8"))
  val socket = new Socket(host, port)
  val writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream, "UTF8"))
  val reader = new BufferedReader(new InputStreamReader(socket.getInputStream, "UTF8"))
  val before = compat.Platform.currentTime
  val ai = new SimpleAI()
  join(botName, botKey, trackName)
  play(1, before, before)



  @tailrec private def play(i0: Int, lastSend: Long, lastRecv: Long) {
    val finish = compat.Platform.currentTime
    val line = reader.readLine()
    val before = compat.Platform.currentTime
//    println("debug: last loop " + (finish-lastRecv) + "ms, full round trip time " + (before-lastRecv) + "ms")
    val i = (i0 + 1) % 60
    if (line != null) {
      log.println(line)
      Serialization.read[TickedMsg](line) match {

        case TickedMsg("gameStart", g, tick: JValue) =>
          println("debug: gameStart")
          ai.throttle match {
            case Some(throttle) => {
              val afterSend = send(lastSend, before, TickedMsg("throttle", throttle, tick))
              play(i, afterSend, before)
            }
            case None => play(i, lastSend, before)
          }


        case TickedMsg("carPositions", data, JNothing) =>
          println("debug: carPositions without gameTick")
          val carPositions = CarPositions(data)
          ai.updatePositions(carPositions)
          ai.throttle match {
            case Some(throttle) => {
              val afterSend = send(lastSend, before, TickedMsg("throttle", throttle, lastRecv))
              play(i, afterSend, before)
            }
            case None => play(i, lastSend, before)
          }


        case TickedMsg("carPositions", data, tick: JValue) =>
          val carPositions = CarPositions(data)
          ai.updatePositions(carPositions)
          ai.throttle match {
            case Some(throttle) => {
              val afterSend = send(lastSend, before, TickedMsg("throttle", throttle, tick))
              play(i, afterSend, before)
            }
            case None => play(i, lastSend, before)
          }

        case TickedMsg("gameInit", data, JNothing) => {
          println("debug: gameInit")
          ai.setRaceData(Race(data))
          ai.throttle match {
            case Some(throttle) => {
              val afterSend = send(lastSend, before, TickedMsg("throttle", throttle, lastRecv))
              play(i, afterSend, before)
            }
            case None => play(i, lastSend, before)
          }
        }

        case TickedMsg("yourCar", data, _) => {
          println("debug: yourCar")
          val myCar = data.extract[CarData]
          ai.myCar = myCar
          play(i, lastSend, before)
        }

        case TickedMsg("lapFinished", data, _) => {
          println("debug: lapFinished: ")
          val lapFinished = LapFinished(data)
          println(lapFinished)
          play(i, lastSend, before)
        }

        case TickedMsg("turboAvailable", data, _) => {
          println("debug: turboAvailable: ")
          val turboAvailable = data.extract[TurboAvailable]
          ai.turbo = Some(turboAvailable)
          play(i, lastSend, before)
        }

        case TickedMsg(msgType, _, _) =>
          println("debug: " + msgType)
          play(i, lastSend, before)
      }
    } else {
      log.flush();
    }
  }

  def send(last: Long, before: Long, msg: TickedMsg): Long = {
    writer.println(Serialization.write(msg))
    writer.flush
    val after = compat.Platform.currentTime
//    println("debug: response sent in " + (after-before) + "ms, since last send " + (after-last) + "ms")
    after
  }

  def join(botName: String, botKey: String, trackName: String) {
    val msg =
      if (trackName.isEmpty) {
        MsgWrapper("join", JoinQuick(botName, botKey))
      } else {
        MsgWrapper("joinRace", JoinRace(BotId(botName, botKey), trackName, "VsSVFO38", 1))
      }
    writer.println(Serialization.write(msg))
    writer.flush
  }

}

