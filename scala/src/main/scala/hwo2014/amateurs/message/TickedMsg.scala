package hwo2014.amateurs.message

import org.json4s._

case class TickedMsg(msgType: String, data: JValue, gameTick: JValue)

object TickedMsg {
  implicit val formats = new DefaultFormats{}

  def apply(msgType: String, data: Any, gameTick: Any): TickedMsg = {
    TickedMsg(msgType, Extraction.decompose(data), Extraction.decompose(gameTick))
  }
}
