package hwo2014.amateurs.message

import org.json4s._

case class MsgWrapper(msgType: String, data: JValue)

object MsgWrapper {
  implicit val formats = new DefaultFormats{}

  def apply(msgType: String, data: Any): MsgWrapper = {
    MsgWrapper(msgType, Extraction.decompose(data))
  }
}
