package hwo2014.amateurs.ai

import hwo2014.amateurs.model.TrackPiece

class SimpleAI extends BaseAI {

  override def throttle: Option[Double] = {
    (distanceLeftFromPiece, nextPiece, currentPiece) match {
      case (Some(distance: Double), Some(nextPiece: TrackPiece), Some(currentPiece: TrackPiece)) => {
        // if the next piece is a curve and we are close to it, or this is a curve and we are far from the end of it
        if ((distance < 40 && nextPiece.isCurve) || (distance > 30 && currentPiece.isCurve)) {
          Some(0.4)
        } else {
          Some(0.85)
        }
      }
      case _ => None
    }
  }
}