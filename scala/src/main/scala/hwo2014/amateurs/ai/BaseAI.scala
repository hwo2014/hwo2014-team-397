package hwo2014.amateurs.ai

import hwo2014.amateurs.model._
import scala.collection.mutable.ListBuffer
import hwo2014.amateurs.model.Track
import hwo2014.amateurs.model.Car
import scala.Some

abstract class BaseAI {

  var raceData: Race = _
  var raceTrack: Track = _
  var cars: List[Car] = _
  var myCar: CarData = _
  var turbo: Option[TurboAvailable] = _

  // holds the history of the positions throughout the game
  val carPositions: ListBuffer[CarPositions] = ListBuffer[CarPositions]()

  def updatePositions(positions: CarPositions) = {
    carPositions += positions
  }

  def setRaceData(race: Race) = {
    raceData = race
    raceTrack = race.track
    cars = race.cars
  }

  def throttle: Option[Double] = {
    Some(0.51)
  }

  def nextPiece: Option[TrackPiece] = {
    myCurrentPosition match {
      case Some(position) => {
        if (position.piecePosition.pieceIndex == raceTrack.pieces.size - 1) {
          Some(raceTrack.pieces(0))
        } else {
          Some(raceTrack.pieces(position.piecePosition.pieceIndex + 1))
        }
      }
      case _ => None
    }
  }

  def currentPiece: Option[TrackPiece] = {
    myCurrentPosition match {
      case Some(position) => {
          Some(raceTrack.pieces(position.piecePosition.pieceIndex))
      }
      case _ => None
    }
  }

  // holds all players current positions
  def currentPositions: Option[CarPositions] =
    if (!carPositions.isEmpty) Some(carPositions.last) else None

  // holds my last position
  def myCurrentPosition: Option[CarPosition] =
    currentPositions match {
      case Some(currentPositions) => currentPositions.positions.find(position => position.id == myCar)
      case _ => None
    }


  def distanceLeftFromPiece: Option[Double] = {
    myCurrentPosition match {
      case Some(position) => {
        val currentPiece = raceTrack.pieces(position.piecePosition.pieceIndex)
        if (currentPiece.angle.isEmpty) { // straight
          Some(currentPiece.length.get - position.piecePosition.inPieceDistance)
        } else { // curve
          val distanceFromCenter = raceTrack.lanes.find(lane => lane.index == myCurrentPosition.get.piecePosition.lane.startLaneIndex).get.distanceFromCenter
          val angle = scala.math.abs(currentPiece.angle.get)
          val pi = scala.math.Pi
          val r = currentPiece.radius.get - distanceFromCenter
          val totalDistance = ((r*pi) / 180) * angle
          Some(totalDistance - position.piecePosition.inPieceDistance)
        }
      }
      case None => None
    }
  }
}

