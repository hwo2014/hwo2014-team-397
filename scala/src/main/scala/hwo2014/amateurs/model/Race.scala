package hwo2014.amateurs.model

import org.json4s._

case class TrackPiece(length: Option[Double], switch: Option[Boolean], radius: Option[Int], angle: Option[Double]) {
  def isCurve = {
    if (radius.isDefined) true else false
  }
  def isStraight = {
    !isCurve
  }
}
case class Position(x: Double, y: Double)
case class StartingPoint(position: Position, angle: Double)
case class Lane(distanceFromCenter: Int, index: Int)
case class CarDimension(length: Double, width: Double, gfp: Double)
case class CarData(name: String, color: String)
case class Car(carData: CarData, carDimensions: CarDimension)
case class RaceSession(laps: Option[Int], maxLapTimeMs: Option[Int], quickRace: Option[Boolean], durationMs: Option[Int])
case class Track(id: String, name: String, pieces: List[TrackPiece], lanes: List[Lane], startingPoint: StartingPoint)

case class Race(track: Track, cars: List[Car], raceSession: RaceSession)
object Race {
  implicit val formats = new DefaultFormats{}
  def apply(data: JValue) = {
    val raceSession = (data \ "race" \ "raceSession").extract[RaceSession]
    val cars = (data \ "race" \ "cars" ).extract[List[Map[String, Map[String, String]]]] map {
      elem => {
        val carDimension = elem.get("dimensions").get
        val carData = elem.get("id").get
        Car(
          CarData(
            carData.get("name").get,
            carData.get("color").get
          ),
          CarDimension(
            carDimension.get("length").get.toDouble,
            carDimension.get("width").get.toDouble,
            carDimension.get("guideFlagPosition").get.toDouble
          )
        )
      }
    }
    val lanes = (data \ "race" \ "track" \ "lanes").extract[List[Lane]]
    val trackId = (data \ "race" \ "track" \ "id").extract[String]
    val trackName = (data \ "race" \ "track" \ "name").extract[String]
    val pieces = (data \ "race" \ "track" \ "pieces").extract[List[TrackPiece]]
    val startingPointPosition = (data \ "race" \ "track" \ "startingPoint" \ "position").extract[Position]
    val startingPointAngle = (data \ "race" \ "track" \ "startingPoint" \ "angle").extract[Double]
    val startingPoint = StartingPoint(startingPointPosition, startingPointAngle)
    val track = Track(trackId, trackName, pieces, lanes, startingPoint)
    new Race(track, cars, raceSession)
  }
}

case class LanePosition(startLaneIndex: Int, endLaneIndex: Int)
case class PiecePosition(pieceIndex: Int, inPieceDistance: Double, lane: LanePosition)
case class CarPosition(id: CarData, angle: Double, piecePosition: PiecePosition)

case class CarPositions(positions: List[CarPosition])
object CarPositions {
  implicit val formats = new DefaultFormats {}

  def apply(data: JValue) = {
    new CarPositions((data).extract[List[CarPosition]])

  }
}

case class LapTime(lap: Int, ticks: Int, millis: Int)
case class RaceTime(laps: Int, ticks: Int, millis: Int)
case class Ranking(overall: Int, fastestLap: Int)
case class LapFinished(carData: CarData, lapTime: LapTime, raceTime: RaceTime, ranking: Ranking)

object LapFinished {
  implicit val formats = new DefaultFormats {}

  def apply(data: JValue) = {
    val car = (data \ "car").extract[CarData]
    val lapTime = (data \ "lapTime").extract[LapTime]
    val raceTime = (data \ "raceTime").extract[RaceTime]
    val ranking = (data \ "ranking").extract[Ranking]
    new LapFinished(car, lapTime, raceTime, ranking)
  }
}

case class TurboAvailable(turboDurationMilliseconds: Double, turboDurationTicks: Double, turboFactor: Double)